#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
#include <cstdlib>
#include <ctime>
using namespace std;
int guessNum(int guess, int range) {
	int input;
	cout << guess << "? (1. correct 2. too high 3. too low)" << '\n';
	cin >> input;
	int newRange = range/2 + range%2;
	switch(input) {
		case 1:
			cout << "number found" << '\n';
			break;
		case 2:
			guessNum((guess - newRange), newRange);
			break;
		case 3:
			guessNum((guess + newRange), newRange);
			break;
		default:
			cout << "invalid option" << '\n';
			guessNum(guess, range);
			break;
	}

}
int guessNum() {
	srand(time( NULL ));
	int guessthis = rand() % 101; 
	int a;                    // Variable declaration
	while(true) {
		cout<<"Enter your guess: ";   // Text Output
		cin>>a;
		if (a == guessthis) {
			cout << "correct" << '\n';
			break;
		}
		else if (a > guessthis) {
			cout << "too high" << '\n';
		}
		else {
			cout << "too low" << '\n';
		}

	}
}
int main()                  // Main function
{
	guessNum(50, 50);
	cout << "Press any key to exit" << '\n';
	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}

