#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
using namespace std;

int main()                  // Main function
{
	int a;                      // Variable declaration
	int i = 0;
	while (i < 10) {
		cout<<"Enter any number other than " << i << '\n';   // Text Output
		cin>>a;
		if (i == a) {
			cout << "Hey! you weren't supposed to enter " << a << "!";
			return 0;
		}
		i++;
	}
	cout << "Wow, you're more patient then I am, you win." << '\n';
	return 0;                   // Obligatory return
}