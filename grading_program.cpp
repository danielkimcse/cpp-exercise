#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
using namespace std;

int main()                  // Main function
{
	int a;                      // Variable declaration
	cout<<"Enter your grade: ";   // Text Output
	cin>>a;
	if (a > 100) {
		cout << "Invalid grade: " << a; 
	}                     // User input
	else if (a >= 90) { //0-59 F 60-69 D 70-79 C 80-89 B 90-100 A
		cout << "You got A";
	}
	else if (a >= 80) {
		cout << "You got B";
	}
	else if (a >= 70) {
		cout << "You got C";
	}
	else if (a >= 60) {
		cout << "You got D";
	}
	else {
		cout << "You got F";
	}
	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}