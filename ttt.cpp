#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
#include <cstring>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#define BOARD_LENGTH 3
char board [BOARD_LENGTH][BOARD_LENGTH];

bool fillTile(int x, int y, char player) {
	if (board[x][y] == ' ') {
		board[x][y] = player;
		return true;
	}
	else {
		return false;
	}
	
}
void createNewGrid() {
	for (int i = 0; i < BOARD_LENGTH; i++) {
		for (int j = 0; j < BOARD_LENGTH; j++) {
			board[i][j] = ' ';
		}
	}
}
void printGrid() { 	//-------
					//|o|x|o|
					//-------
					//|o|x|x|
					//-------
					//|o|o|o|
					//-------
	cout << "-------" << '\n';
	for (int i = 0; i < BOARD_LENGTH; i++) {
		cout << "|";
		for (int j = 0; j < BOARD_LENGTH; j++)  {
			cout << board[i][j] << "|";
		}
		cout << '\n';
		cout << "-------" << '\n';
	}
}
bool checkWin(char player) {
	int ic[3] = {0};
	int jc[3] = {0};
	for (int i = 0; i < BOARD_LENGTH; i++) {
		for (int j = 0; j < BOARD_LENGTH; j++) {
			if (board[i][j] == player) {
				++ic[i];
				++jc[j];
				if(ic[i] == 3) {
					return true;
				}
				if(jc[j] == 3) {
					return true;
				}
			}
		}
		if ((board[0][0] == player && board[1][1] == player && board[2][2] == player) || 
			(board[2][0] == player && board[1][1] == player && board[0][2] == player)) {
			return true;
		}
	}	
	return false;
}
int main()                  // Main function
{
	srand(time( NULL ));
	createNewGrid();
	int i = 0;
	while (i < BOARD_LENGTH * BOARD_LENGTH) {
		int x;
		int y;
		char p;
		if (i % 2) {
			p = 'x';
			cout << "Player " << p << "'s move" << endl;
			cout << "Enter x (0-2)" << endl;
			cin >> x;
			cout << "Enter y (0-2)" << endl;
			cin >> y;
			while(!fillTile(x,y,p)) {
				cout << "Invalid move" << endl;
				cout << "Enter x (0-2)" << endl;
				cin >> x;
				cout << "Enter y (0-2)" << endl;
				cin >> y;
			}
		}
		else {
			p = 'o';
			cout << "Player " << p << "'s move" << endl;
			x = rand() % 3;
			y = rand() % 3;
			while(!fillTile(x,y,p)) {
				x = rand() % 3;
				y = rand() % 3;
			}
		}
		printGrid();
		if (checkWin(p)) {
			cout << p << " wins" << endl;
			return 0;
		}
		i++;
	}
	cout << "draw" << endl;
	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}