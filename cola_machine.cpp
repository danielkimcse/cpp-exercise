#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
using namespace std;

int main()                  // Main function
{
	int a;                      // Variable declaration
	cout<<"Select a beverage" << '\n' << "1. Coke 2. Water 3. Sprite 4. Mountain Dew 5. Fanta (1-5) ";   // Text Output
	cin>>a;
	switch(a) {
		case 1 : 
			cout << "Coke dispensed" << '\n';
			break;
		case 2 : 
			cout << "Water dispensed" << '\n';
			break;
		case 3 : 
			cout << "Sprite dispensed" << '\n';
			break;
		case 4 : 
			cout << "Mountain Dew dispensed" << '\n';
			break;
		case 5 : 
			cout << "Fanta dispensed" << '\n';
			break;
		default : 
			cout << "Error. choice was not valid, here is your money back." << '\n';
	}
	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}