#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
#include <cstdlib>
#include <ctime>
using namespace std;

#define BOARD_LENGTH 10
#define UP 0
#define LEFT 1
#define RIGHT 2
#define DOWN 3
char board [BOARD_LENGTH][BOARD_LENGTH];

class Unit {
	public:
		void move(int direction);
		int getX();
		int getY();
		char getName();
		void setX(int x);
		void setY(int y);
		void setName(char name);
		void setFatal(bool b);
		void setMonster(bool b);
		void setPlayer(bool b);
		void setTreasure(bool b);
		bool isFatal();
		bool isMonster();
		bool isPlayer();
		bool isTreasure();
		Unit(int x, int y, char name);
	protected:
		int x;
		int y;
		char name;
		bool fatal;
		bool monster;
		bool player;
};
bool updateBoard(int x, int y, char name) {
	if (board[x][y] == '.') {
		cout << "hello" << endl;
		board[x][y] = name;
		return true;
	}
	else {
		return false;
	}
}
Unit :: Unit(int x, int y, char name) {
	this -> setX(x);
	this -> setY(y);
	this -> setName(name);
	updateBoard(x,y,name);
}
void Unit::move(int direction) {
	if (direction == UP) {
		if (y > 0) y--;
	}
	else if (direction == LEFT) {
		if (x > 0) x--;
	}
	else if (direction == RIGHT) {
		if (x < BOARD_LENGTH) x++;
	}
	else if( direction == DOWN) {
		if (y < BOARD_LENGTH) y++;
	}
	updateBoard(x,y,*this);
}
int Unit::getX() {
	return x;
}
int Unit::getY() {
	return y;
}
char Unit::getName() {
	return name;
}
void Unit::setX(int x) {
	this -> x = x;
}
void Unit::setY(int y) {
	this -> y = y;
}
void Unit::setFatal(bool b) {
	this -> fatal = b;
}
void Unit::setMonster(bool b) {
	this -> monster = b;
}
void Unit::setPlayer(bool b) {
	this -> player = b;
}
void Unit::setName(char name) {
	this -> name = name;
}
bool Unit::isFatal() {
	return fatal;
}
bool Unit::isMonster() {
	return monster;
}
bool Unit::isPlayer() {
	return player;
}


void createNewGrid() {
	for (int i = 0; i < BOARD_LENGTH; i++) {
		for (int j = 0; j < BOARD_LENGTH; j++) {
			board[i][j] = '.';
		}
	}
}
void printGrid() { 	//-------
					//|o|x|o|
					//-------
					//|o|x|x|
					//-------
					//|o|o|o|
					//-------
	for (int i = 0; i < BOARD_LENGTH; i++) {
		for (int j = 0; j < BOARD_LENGTH; j++)  {
			cout << board[i][j] << " ";
		}
		cout << '\n';
	}
}
int main()                  // Main function
{
	srand(time( NULL ));
	createNewGrid();
	Danny danny(0,0,'D');
	Treasure treasure(9,9,'X');
	printGrid();
	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}