#include <iostream>        // I/O header file for C++
#include <conio.h>           // Header file for getch();
using namespace std;

int main()                  // Main function
{
	int a;                      // Variable declaration
	int i = 0;
	int people [10];
	while (i < 10) {
		int a;
		cout<<"Enter the number of pancakes eaten for breakfast by Person " << i+1 << '\n';   // Text Output
		cin>>a;
		people[i] = a;
		i++;
	}
	i = 0;
	while (i < 10) {
		cout << "Person " << i+1 << " ate " << people[i] << " pancakes" << '\n';
		i++;
	}

	getch();                    // Pausing the screen until user key-press
	return 0;                   // Obligatory return
}